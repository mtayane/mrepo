package steps;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.vhv.cucumber.HomePage;
import de.vhv.cucumber.LoginPage;
import de.vhv.cucumber.TestBase;

public class LoginStep extends TestBase {

	LoginPage lp = new LoginPage(getDriver());
	HomePage hp = new HomePage(getDriver());

	@Before
	public void setUp() {
		setUP();
	}

	@After
	public void tearDown(Scenario scenario) {
		System.out.println(scenario.isFailed());
		if (scenario.isFailed()) {
			byte[] screenshotBytes = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshotBytes, "image/png");
		}
		 getDriver().quit();
	}
	@Test
	
	@Given("^der Benutzer ruft die gmail-Webseite auf$")
	public void der_Benutzer_ruft_die_gmail_Webseite_auf() throws Throwable {
		
		getUrl("http://gmail.com");
	}
	
	@When("^der Benutzer gibt den Benutzernamen und das Passwort ein$")
	public void der_Benutzer_gibt_den_Benutzernamen_und_das_Passwort_ein(DataTable table) throws Throwable {

		List<List<String>> data = table.raw(); 
		lp.username.sendKeys(data.get(1).get(0));
		Thread.sleep(2000);
		lp.submitBtn.click();
		Thread.sleep(2000);
		lp.pw.sendKeys(data.get(1).get(1));
	}

	@When("^der Benutzer clickt auf submit button$")
	public void der_Benutzer_clickt_auf_submit_button() throws Throwable {
		lp.submitBtn.click();
	}

	@Then("^der Benutzer ist erfolgreich eingeloggt$")
	public void der_Benutzer_ist_erfolgreich_eingeloggt()  {
		try {
			Thread.sleep(2000);
			Assert.assertTrue(lp.forTheAssertion.isDisplayed());
			System.out.println("Login erfolgreich");
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Thread.sleep(4000);
		//Actions action = new Actions(getDriver());
		//action.sendKeys(Keys.ESCAPE).build().perform();
	}

	@When("^der Benutzer clickt auf dropdown menu$")
	public void der_Benutzer_clickt_auf_dropdown_menu() throws Throwable {
		Thread.sleep(2000);
		hp.LogOutMenu.click();
	}

	@When("^der Benutzer clickt auf logOut button$")
	public void der_Benutzer_clickt_auf_logOut_button() throws Throwable {
		Thread.sleep(2000);
		hp.LogOutBtn.click();
	}

	@Then("^der benutzer ist ausgeloggt$")
	public void der_benutzer_ist_ausgeloggt() throws Throwable {
		Thread.sleep(2000);
		Assert.assertTrue(lp.pw.isDisplayed());
	}

	@Then("^der Benutzer befindet sich auf der Loginpage wieder$")
	public void der_Benutzer_befindet_sich_auf_der_Loginpage_wieder() throws Throwable {
		System.out.println("der Benutzer ist erfolgreich ausgeloggt");
	}
	
	@Then("^der Benutzer ist nicht eingeloggt$")
	public void der_Benutzer_ist_nicht_eingeloggt() throws Throwable {
		System.out.println("der Login ist fehlgeschlagen");
	   
	}

	@Then("^ein Fehlermeldung ist aufgetreten$")
	public void eine_Fehlermeldung_ist_aufgetreten() throws Throwable {
		Thread.sleep(2000);
		Assert.assertTrue(lp.LoginFail.isDisplayed());
		System.out.println("Die Fehlermeldung wurde angezeitgt");
	    
	}

}
