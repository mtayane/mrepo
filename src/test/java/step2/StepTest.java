package step2;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.vhv.cucumber.HomePage;
import de.vhv.cucumber.LoginPage;
import de.vhv.cucumber.TestBase;


public class StepTest extends TestBase {

	LoginPage lp = new LoginPage(getDriver());
	HomePage hp = new HomePage(getDriver());

	@Before
	public void setUp() {
		setUP();
	}

	@After
	public void tearDown(Scenario scenario) {
		System.out.println(scenario.isFailed());
		if (scenario.isFailed()) {
			byte[] screenshotBytes = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshotBytes, "image/png");
		}
		getDriver().quit();
	}

	@Test
	@Given("^der user ruft die gmail-Webseite auf$")
	public void der_user_ruft_die_gmail_Webseite_auf() throws Throwable {
		getUrl("http://gmail.com");
	}

	@When("^der user gibt den Benutzernamen und das Passwort ein$")
	public void der_user_gibt_den_Benutzernamen_und_das_Passwort_ein(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		lp.username.sendKeys(data.get(1).get(0));
		Thread.sleep(2000);
		lp.submitBtn.click();
		Thread.sleep(2000);
		lp.pw.sendKeys(data.get(1).get(1));
	}

	@When("^der user clickt auf submit button$")
	public void der_user_clickt_auf_submit_button() throws Throwable {

		lp.submitBtn.click();
	}

	@Then("^der user ist erfolgreich eingeloggt$")
	public void der_user_ist_erfolgreich_eingeloggt() throws Throwable {

		try {
			Thread.sleep(2000);
			Assert.assertTrue(lp.forTheAssertion.isDisplayed());
			System.out.println("Login erfolgreich");

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Thread.sleep(5000);
//		Actions action = new Actions(getDriver());
//		action.sendKeys(Keys.ESCAPE).build().perform();
	}

	@When("^der user auf SchreibenButton clickt$")
	public void der_user_auf_SchreibenButton_clickt() throws Throwable {
		Thread.sleep(2000);
		hp.emailSchreibenButton.click();
	}

	@Then("^oeffnet sich ein NeueNachrihtFenster$")
	public void oeffnet_sich_ein_NeueNachrihtFenster() throws Throwable {
		Thread.sleep(2000);
		Assert.assertTrue(hp.neueNachrichtForTheAssertion.isDisplayed());
	}

	@When("^der user email von dem Empfaenger und den Betreff eingibt$")
	public void der_user_email_von_dem_Empfaenger_und_den_Betreff_eingibt(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		hp.empfängerField.sendKeys(data.get(1).get(0));
		hp.betreffField.sendKeys(data.get(1).get(1));
	}
	@And("^der user eine Nachricht eingibt$")
	public void der_user_eine_Nachricht_eingibt(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		hp.neueNachrichtEingabe.sendKeys(data.get(1).get(0));

	}

	@Then("^wird die Nachricht angezeigt$")
	public void wird_die_Nachricht_angezeigt() throws Throwable {

		hp.sendButton.click();
	}

	@When("^der user clickt auf dropdown menu$")
	public void der_user_clickt_auf_dropdown_menu() throws Throwable {

		Thread.sleep(2000);
		hp.LogOutMenu.click();
	}

	@When("^der user clickt auf logOut button$")
	public void der_user_clickt_auf_logOut_button() throws Throwable {

		Thread.sleep(2000);
		hp.LogOutBtn.click();
//		Thread.sleep(4000);
//		Actions action = new Actions(getDriver());
//		action.sendKeys(Keys.ESCAPE).build().perform();
	}

	@Then("^der user ist ausgeloggt$")
	public void der_user_ist_ausgeloggt() throws Throwable {

		Thread.sleep(2000);
		Assert.assertTrue(lp.pw.isDisplayed());
	}

	@Then("^der user befindet sich auf der Loginpage wieder$")
	public void der_user_befindet_sich_auf_der_Loginpage_wieder() throws Throwable {

		System.out.println("der Benutzer ist erfolgreich ausgeloggt");
	}

	@Then("^der user ist nicht eingeloggt$")
	public void der_user_ist_nicht_eingeloggt() throws Throwable {

		System.out.println("Der Login ist fehlgeschlagen");

	}

	@Then("^eine Fehlermeldung ist aufgetreten$")
	public void eine_Fehlermeldung_ist_aufgetreten() throws Throwable {

		Thread.sleep(2000);
		Assert.assertTrue(lp.LoginFail.isDisplayed());
		System.out.println("Die Fehlermeldung wurde angezeitgt");
	}
}
