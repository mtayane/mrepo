package de.vhv.cucumber;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using="//span[@class = 'gb_ya gbii']")
	public WebElement LogOutMenu;
	
	@FindBy(how = How.XPATH, using="//a[@class = 'gb_0 gb_Ef gb_Mf gb_le gb_kb']")
	public WebElement LogOutBtn;
	
	@FindBy(how = How.ID, using="//span[@class = 'RveJvd snByac' and text() = 'Erneut anmelden']")
	public WebElement forTheAssertion;
	
	@FindBy(how = How.XPATH, using = "//div[text() = 'Schreiben']")
    public WebElement emailSchreibenButton;	
	
	@FindBy(how = How.XPATH, using = "	//div[text() = 'Neue Nachricht']")
    public WebElement neueNachrichtForTheAssertion;
	
	@FindBy(how = How.XPATH, using = "//div[@class = 'Am Al editable LW-avf']")
    public WebElement neueNachrichtEingabe;
	
	@FindBy(how = How.XPATH, using = "//textarea[@name='to']")
    public WebElement empfängerField;
	
	@FindBy(how = How.XPATH, using = "//input[@name= 'subjectbox']")
    public WebElement betreffField;
	
	@FindBy(how = How.XPATH, using = "//div[@id= ':qn']")
    public WebElement sendButton;
	
	
	
	
	
	
	
	
}


